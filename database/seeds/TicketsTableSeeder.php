<?php

use Illuminate\Database\Seeder;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 0; $i<20; $i++) {
        DB::table('tickets')->insert([

            'user_id' => 1,

            'title' => str_random(40),

            'description' => str_random(150),
        ]);
        }
    }
}

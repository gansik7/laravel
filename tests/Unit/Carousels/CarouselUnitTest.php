<?php
namespace Tests\Unit\Carousels;
use Tests\TestCase;

use App\Shop\Carousels\Carousel;
use App\Shop\Carousels\Repositories\CarouselRepository;


class CarouselUnitTest extends TestCase
{
    /** @test */
    public function it_can_create_a_carousel()
    {
        $data = [
            'title' => 'title',
            'link' => 'link',
            'src' => 'src',
        ];

        $carouselRepo = new CarouselRepository(new Carousel);
        $carousel = $carouselRepo->createCarousel($data);

        $this->assertInstanceOf(Carousel::class, $carousel);
        $this->assertEquals($data['title'], $carousel->title);
        $this->assertEquals($data['link'], $carousel->link);
        $this->assertEquals($data['src'], $carousel->src);
    }
}

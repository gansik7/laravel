<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// First Route method – Root URL will match this method

Route::get('/', function () {

    return view('welcome');

});

// Second Route method – Root URL with ID will match this method

Route::get('ID/{id}',function($id){

    echo 'ID: '.$id;

});

// Third Route method – Root URL with or without name will match this method

Route::get('/user/{name?}',function($name = 'Virat Gandhi'){

    echo "Name: ".$name;

});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Create new ticket
Route::get('/create/ticket','TicketController@create');
Route::post('/create/ticket','TicketController@store');
Route::get('/tickets', 'TicketController@index');
Route::get('/edit/ticket/{id}','TicketController@edit');
Route::get('/view/ticket/{id}','TicketController@view');
Route::patch('/edit/ticket/{id}','TicketController@update');
Route::delete('/delete/ticket/{id}','TicketController@destroy');
/*
Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('products','ProductController');
});

Route::get('localization/{locale}','LocalizationController@index');
Route::get('/admin', 'AdminController@index');
*/

Route::get('/validation','ValidationController@showform');
Route::post('/validation','ValidationController@validateform');
Route::resource('items', 'ItemController');
Route::get('/cookie/set','CookieController@setCookie');
Route::get('/cookie/get','CookieController@getCookie');

Route::get('session/get','SessionController@accessSessionData');
Route::get('session/set','SessionController@storeSessionData');
Route::get('session/remove','SessionController@deleteSessionData');
